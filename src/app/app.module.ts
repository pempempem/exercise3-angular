import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import {RouterModule} from '@angular/router'; 
import {FormsModule} from '@angular/forms'; 
import {HttpModule} from '@angular/http';
import { CzytelnikComponent } from './czytelnik/czytelnik.component';
import { EdycjaComponent } from './edycja/edycja.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CzytelnikComponent,
    EdycjaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    HttpModule, 
    RouterModule.forRoot([ 
      {path: "", component: HomeComponent}, 
      {path: "czytelnik", component: CzytelnikComponent},
      {path: "edycja/:id", component: EdycjaComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
