import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CzytelnikComponent } from './czytelnik.component';

describe('CzytelnikComponent', () => {
  let component: CzytelnikComponent;
  let fixture: ComponentFixture<CzytelnikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CzytelnikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CzytelnikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
