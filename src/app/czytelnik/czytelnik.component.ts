import {
  Component,
  OnInit
} from '@angular/core';
import {
  Http,
  Response,
  Headers
} from '@angular/http';
@Component({
  selector: 'app-czytelnik',
  templateUrl: './czytelnik.component.html',
  styleUrls: ['./czytelnik.component.css']
})
export class CzytelnikComponent implements OnInit {
  constructor(private http: Http) {}
  potwierdzenieDodania: string = "Dodano nowego czytelnika";
  zostalDodany: boolean = false;
  czytelnikObj: object = {};
  dodajNowegoCzytelnika = function (czytelnik) {
    this.czytelnikObj = {
      "imie": czytelnik.imie,
      "nazwisko": czytelnik.nazwisko,
      "klasa": czytelnik.klasa
    }
    this.http.post("http://localhost:3000/czytelnicy/", this.czytelnikObj).subscribe(response => this.zostalDodany = true)
  }
  ngOnInit() {}
}
