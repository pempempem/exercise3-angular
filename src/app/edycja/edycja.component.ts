import {
  Component,
  OnInit
} from '@angular/core';
import {
  Http,
  Response,
  Headers
} from '@angular/http';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-edycja',
  templateUrl: './edycja.component.html',
  styleUrls: ['./edycja.component.css']
})
export class EdycjaComponent implements OnInit {

  constructor(private http: Http,private route: ActivatedRoute) {};

  private headers = new Headers({
    'Content-Type': 'application/json'
  });
  id: number;
  potwierdzenieDodania: string = "Czytelnik został edytowany";
  zostalEdytowany: boolean = false;
  czytelnik : object = {};
  edytowanyCzytelnik : object = {};

  pobierzDane = function () {
    const url = `${"http://localhost:3000/czytelnicy/"}/${this.id}`;
    this.http.get(url, {headers: this.headers}).subscribe(response => this.czytelnik = response.json())
  }

  edytujCzytelnika = function (czytelnikZForm) {
    const url = `${"http://localhost:3000/czytelnicy"}/${this.id}`;
    this.edytowanyCzytelnik = {
      "imie": czytelnikZForm.imie,
      "nazwisko": czytelnikZForm.nazwisko,
      "klasa": czytelnikZForm.klasa
    }

    console.log(this.edytowanyCzytelnik);
    return this.http.put(url, this.edytowanyCzytelnik).subscribe(response => this.zostalDodany = true)
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.pobierzDane();
  }

}
