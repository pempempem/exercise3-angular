import {
  Component,
  OnInit
} from '@angular/core';
import {
  Http,
  Response,
  Headers
} from '@angular/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  constructor(private http: Http) {}
  id: number;
  private headers = new Headers({
    'Content-Type': 'application/json'
  });
  czytelnicy = [];
  pobierzDane = function () {
    this.http.get("http://localhost:3000/czytelnicy").subscribe(response => this.czytelnicy = response.json())
  }
  usunCzytelnika = function (id) {
    if (confirm("Usunac czytelnika?")) {
      const url = `${"http://localhost:3000/czytelnicy"}/${id}`;
      return this.http.delete(url, {
        headers: this.headers
      }).toPromise().then(() => {
        this.pobierzDane();
      })
    }
  }
  ngOnInit() {
    this.pobierzDane();
  }
}
